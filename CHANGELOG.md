# Changelog

## Version 1.2.0
- added cc-recipients

## Version 1.1.1

- added support for anonymous authentication

## Version 1.1.0

- made the main template file optional
- added reply-to field
