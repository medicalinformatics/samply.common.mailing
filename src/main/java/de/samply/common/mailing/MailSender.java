/*
 * Copyright (c) 2017. Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.common.mailing;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import de.samply.config.util.JAXBUtil;
import de.samply.string.util.StringUtil;

/**
 * Used to send emails and load the therefore 
 * needed email sending configuration.
 */
public class MailSender {
    
    private final Session mailSession;
    public static final String CONFIG_FILE_NAME = "mailSending.xml";
    private final MailSending mailSending;
    
    /**
     * This constructor uses an already loaded mail sending configuration.
     * The configuration can be loaded by 
     * {@link #loadMailSendingConfig(java.lang.String) }.
     * @param mailSending Configuration for mail sending
     */
    public MailSender(MailSending mailSending) {
        this.mailSending = mailSending;
        this.mailSession = getMailSessionFromConfig(mailSending);
    }
    
    /**
     * Creates mail session from settings in configuration file.
     * @return created {@link Session}
     */
    private static Session getMailSessionFromConfig(MailSending mailSending) {
        Properties mailProperties = new Properties();

        String protocol = mailSending.getProtocol();

        mailProperties.setProperty("type", "transport");
        mailProperties.setProperty("mail.transport.protocol", protocol);
        mailProperties.setProperty("mail.host", mailSending.getHost());

        int port = mailSending.getPort();
        if (port == 0) {
            port = 25;
        }
        mailProperties.setProperty("mail." + protocol + ".port", "" + port);

        return Session.getInstance(mailProperties);
    }
    
    /**
     * Sends the email using the settings specified in the configuration file.
     * @param email an email to be sent
     */
    public void send(OutgoingEmail email) {
        MimeMessage message = new MimeMessage(mailSession);
        try {
            for (String addressee : email.getAddressees()) {
                message.addRecipient(RecipientType.TO, 
                        new InternetAddress(addressee));
            }

            for (Address ccRecipient : email.getCcRecipient()) {
                message.addRecipient(RecipientType.CC, ccRecipient);
            }
            
            if (email.getReplyTo() != null && email.getReplyTo().size() > 0) {
                message.setReplyTo(email.getReplyToArray());
            }
            message.setFrom(getFromAddress());
            message.setSubject(email.getSubject());
            message.setContent(email.getText(), "text/plain; charset=utf-8");
            message.saveChanges();
            Transport tr = mailSession.getTransport();


            // Connect anonymously if necessary.
            if(StringUtil.isEmpty(mailSending.getUser())) {
                tr.connect();
            } else {
                tr.connect(mailSending.getUser(), mailSending.getPassword());
            }
            tr.sendMessage(message, message.getAllRecipients());
            tr.close();

        } catch (MessagingException ex) {
            LoggerFactory.getLogger(MailSender.class).error("Exception: ", ex);
        }
    }
    
    /**
     * Loads the mail sending configuration from the XML file.
     * @param projectName name of the folder to look for config
     * @return the created {@link MailSending} object
     * @see JAXBUtil#findUnmarshall(java.lang.String, javax.xml.bind.JAXBContext, java.lang.Class, java.lang.String) 
     */
    public static MailSending loadMailSendingConfig(String projectName) {
        try {
            return JAXBUtil.findUnmarshall(CONFIG_FILE_NAME,
                    JAXBContext.newInstance(ObjectFactory.class),
                    MailSending.class, projectName);

        } catch (FileNotFoundException | JAXBException | SAXException |
                ParserConfigurationException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    /**
     * Create sender address from settings 
     * using mail address and optional display name.
     * @return created address
     */
    private InternetAddress getFromAddress() {
        try {
            return new InternetAddress(
                    mailSending.getFromAddress(),
                    mailSending.getFromName(),
                    "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(
                    "Exception while setting from-address for mail sending", ex);
        }
    }
    
}
